/*

1. What built-in JS keyword is used to import packages?
    ans. 
        require("defaultPackage")

2. What Node.js module/package contains a method for server creation?
    ans.
        "http"


3. What is the method of the http object responsible for creating a server using Node.js?
    ans.
        .createServer(function (req, res){}


4. Between server and client, which creates a request?
    ans.
        client

5. Between server and client, which creates a response? 
    ans.
        server

6. What is a runtime environment used to create stand-alone backend applications written in Javascript?
    ans.
        Node.js

7.What is the largest registry for Node packages?
    ans.
        Node Package Manager (npm)

*/
